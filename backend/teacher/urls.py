from django.urls import path
from .views import *

app_name = 'teacher'

urlpatterns = [
    path('api/v1/teachers/', TeacherListCreateView.as_view(), name='teacher-list'),
    path('api/v1/teacher/<int:pk>/', TeacherDetailUpdateDeleteView.as_view(), name='teacher-detail'),
]
