from datetime import date
from rest_framework import serializers
from .models import Teacher


# Teacher detail serializer
class TeacherDetailSerializer(serializers.ModelSerializer):
    age = serializers.SerializerMethodField()

    class Meta:
        model = Teacher
        fields = '__all__'


# Teacher create serializer
class CreateTeacherSerializer(serializers.ModelSerializer):

    class Meta:
        model = Teacher
        exclude = ['status', 'created_at', 'updated_at']
