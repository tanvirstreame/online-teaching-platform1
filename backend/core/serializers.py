from rest_framework import serializers
from dj_rest_auth.registration.serializers import RegisterSerializer
from .models import User


class UserSerializer(serializers.ModelSerializer):
    # password2 = serializers.CharField(style={'input_type':'password'}, write_only=True)

    class Meta:
        model = User
        fields = [
            'full_name',
            'username',
            'email',
            'phone',
            'picture',
            'role',
            'password',
            # 'password2'
        ]
        extra_kwargs = {
            'password': {
                'write_only': True
            }
        }

class LoginSerializer(serializers.ModelSerializer):
    # password2 = serializers.CharField(style={'input_type':'password'}, write_only=True)

    class Meta:
        model = User
        fields = [
            'email',
            'password'
        ]
       
