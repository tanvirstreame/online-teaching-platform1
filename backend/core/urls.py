from django.urls import path, include
from rest_framework_simplejwt import views as jwt_views
from core.views import (
    GoogleLogin,
    RegistrationView,
    index
)

urlpatterns = [
    path('', index, name="index"),
    path('api/v1/registration/', RegistrationView.as_view(), name="registration"),
    path('api/v1/google/', GoogleLogin.as_view(), name='google-auth'),
    path('api/v1/accounts/', include('allauth.urls'), name='socialaccount_signup'),
    path('api/v1/login/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/v1/token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
]
