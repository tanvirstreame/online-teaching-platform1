import React, { useState, useEffect } from "react";
import fakeData from "../FakeData/fakeData";
import image from "../assets/teacher.jpg";
import { Link } from "react-router-dom";

const TeacherList = () => {

    const [teacher, setTeacher] = useState([]);

    useEffect(() => {
        setTeacher(fakeData);
    }, [])

    return (
        <div className="container teacher-component">
            <div className="row">
                {
                    teacher.map((eachTeacher, index) => (
                        <Link className="col-md-3 teacher-card-link" to={`teacher-info/${eachTeacher.id}`}>
                            <div className="card mb-4">
                                <img class="card-img-top" src={image} alt="Card image cap" />
                                <div className="card-body">
                                    <div>
                                        <b>Name: </b>
                                        <span>{eachTeacher.first_name} </span>
                                        <span>{eachTeacher.last_name}</span>
                                    </div>
                                    <p><b>Email: </b><span>{eachTeacher.email}</span></p>
                                </div>
                            </div>
                        </Link>
                    ))
                }
            </div>
        </div>
    )
}

export default TeacherList;